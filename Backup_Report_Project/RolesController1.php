<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\IRole;
use App\Repositories\Eloquent\Criteria\LatestFirst;
use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use App\Services\UserService;
use App\Models\Role;

class RolesController1 extends Controller
{
    protected $roles;

    public function __construct(IRole $roles)
    {
        $this->middleware('auth');
        $this->roles = $roles;
    }

    public function getIndex(Request $request)
    {
        if ($request->user()->can('manage-role')) {
            return view('front-end.role.index');
        }
        return redirect()->back()->with([
            'flash_level'   => 'warning',
            'flash_message' => trans('action.role_log')
        ]);
    }
    public function index()
    {
        return $this->roles->withCriteria(
            new LatestFirst()
        )->datatable();
    }

    public function getAdd(Request $request)
    {
        if ($request->user()->can('manage-role')) {
            $permissions = config('permission'); //get permission array
            return view('front-end.role.add', compact('permissions'));
        }
        return redirect()->back()->with([
            'flash_level'   => 'warning',
            'flash_message' => trans('action.role_log')
        ]);
    }

    public function postAdd(RoleRequest $request)
    {
        $this->roles->create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'permission' => json_encode($request->input('permissions')),
            'created_by' => auth()->user()->name
        ]);

        return redirect()->route('front-end.role.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.add_success')
        ]);
    }

    public function getEdit($id, Request $request)
    {
        if ($request->user()->can('manage-role')) {
            $item = $this->roles->find($id);
            $permissions = config('permission'); //get permission array
            $role_permission = $item->getPermission();
            return view('front-end.role.edit', compact('item', 'permissions', 'role_permission'));
        }
        return redirect()->back()->with([
            'flash_level'   => 'warning',
            'flash_message' => trans('action.role_log')
        ]);
    }

    public function postEdit($id, RoleRequest $request)
    {

        $this->roles->update($id, [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'permission' => json_encode($request->input('permissions')),
            'created_by' => auth()->user()->name
        ]);
        return redirect()->route('front-end.role.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.update_success')
        ]);
    }

    public function postDelete(Request $request)
    {
        if ($request->user()->can('manage-role')) {
            $role = $this->roles->find($request->id);

            if ($role->users->count() > 0) {
                return response()->json([
                    'status' => 403,
                    'message' => trans('role.cannot_del_role')
                ]);
            } else {
                $this->roles->delete($request->id);
                return response()->json([
                    'status' => 200
                ]);
            }
        }
        return response()->json([
            'status' => 403,
            'message' => trans('action.role_log')
        ]);
    }
}
