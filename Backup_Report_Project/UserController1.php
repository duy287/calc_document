<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\IGroup;
use App\Repositories\Contracts\IRole;
use App\Repositories\Contracts\IUser;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Auth;
use App\Repositories\Eloquent\Criteria\{LatestFirst};

class UserController1 extends Controller
{
    protected $users;
    protected $roles;
    protected $groups;
    public function __construct(IUser $users, IRole $roles, IGroup $groups)
    {
        $this->middleware('auth');
        $this->users = $users;
        $this->roles = $roles;
        $this->groups = $groups;
    }

    public function getList(Request $request)
    {
        if ($request->user()->can('manage-user')) {
            return view('front-end.user.index');
        }
        return redirect()->back()->with([
            'flash_level'   => 'warning',
            'flash_message' => trans('action.role_log')
        ]);
    }
    public function index()
    {
        return $this->users->withCriteria(
            new LatestFirst()
        )->datatable();
    }

    public function getAdd(Request $request)
    {
        if ($request->user()->can('manage-user')) {
            $roles = $this->roles->all();
            return view('front-end.user.add', compact('roles'));
        }
        return redirect()->back()->with([
            'flash_level'   => 'warning',
            'flash_message' => trans('action.role_log')
        ]);
    }

    public function postAdd(UserRequest $request)
    {
        $data = $request->except(['_token']);
        $data['password'] = bcrypt($request->password);
        $this->users->create($data);
        return redirect()->route('front-end.user.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.add_success')
        ]);
    }

    public function getEdit($id, Request $request)
    {
        if ($request->user()->can('manage-user')) {
            $roles = $this->roles->all();
            $item = $this->users->find($id);
            return view('front-end.user.edit', compact('item', 'roles'));
        }
        return redirect()->back()->with([
            'flash_level'   => 'warning',
            'flash_message' => trans('action.role_log')
        ]);
    }

    public function postEdit($id, UserRequest $request)
    {
        $data = $request->except(['_token']);
        $this->users->update($id, $data);
        return redirect()->route('front-end.user.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.update_success')
        ]);
    }

    public function postDelete(Request $request)
    {
        if ($request->user()->can('manage-user')) {
            $user_current_id = Auth::id();
            $user = $this->users->find($request->id);
            if ($user_current_id == $user->id) {
                return response()->json([
                    'status' => 0,
                ]);
            } else {
                $this->groups->deleteUser($user);
                $this->users->delete($request->id);
                return response()->json([
                    'status' => 1
                ]);
            }
        }
        return response()->json([
            'status' => 403,
            'message' => trans('action.role_log'),
        ]);
    }

    public function profile()
    {
        $data = $this->users->find(auth()->id());
        $groups_name = [];
        $groups = $this->groups->all();
        foreach ($groups as $group) {
            $user_ids = (array) json_decode($group->user_ids);
            if (in_array($data->id, $user_ids)) {
                $groups_name[] = $group->name;
            }
        }
        return view('front-end/profile/index', [
            'data' => $data,
            'groups_name' => $groups_name
        ]);
    }

    //Change Profile user
    public function changeProfile(Request $request, $id)
    {
        $validator = $this->users->ValidateUserEdit($request);
        if (request()->ajax()) {
            if ($validator->fails()) {
                return response()->json([
                    'status' => 422,
                    'errors' => $validator->errors()
                ]);
            } else {
                $this->users->updateProfile($request);
                return response()->json([
                    'satus' => 500,
                    'message' => trans('action.update_success')
                ]);
            }
        } else {
            if ($validator->fails()) {
                return redirect('/profile')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $this->users->updateProfile($request);
                return redirect('/profile')->with('status', trans('admin/layout.edit_sucss'));
            }
        }
    }

    public function changeAvatar(Request $request, $id)
    {
        $this->users->updateProfile($request);
        return response()->json([
            'satus' => 500,
            'message' => trans('action.update_success')
        ]);
    }
    //Change password user
    public function changePassword(Request $request, $id)
    {
        $validator = $this->users->ValidateUserPass($request);
        if (request()->ajax()) {
            if ($validator->fails()) {
                return response()->json([
                    'status' => 422,
                    'errors' => $validator->errors()
                ]);
            } else {
                $this->users->ChangePassword($request);
                return response()->json([
                    'satus' => 500,
                    'message' => trans('action.update_success')
                ]);
            }
        }
        if ($validator->fails()) {
            return redirect('/profile')
                ->withErrors($validator)
                ->withInput();
        } else {
            $this->users->ChangePassword($request);
            return redirect('/profile')->with([
                'flash_level'   => 'success',
                'flash_message' => trans('action.update_success')
            ]);
        }
    }




    public function getDataUsers()
    {
        $query = $this->users->all();
        return response()->json($query);
    }
}
