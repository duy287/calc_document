<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Services\UserService;

class UserManagementController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->middleware('auth');
        $this->userService = $userService;
    }
    //api role
    public function getRoles()
    {
        return $this->userService->getRoles();
    }
    //api users
    //---Using Datatable
    public function getUsers()
    {
        return $this->userService->getUsers();
    }
    //--- Conver to Json
    public function getDataUsers()
    {
        return $this->userService->getDataUsers();
    }
    //api group
    public function getGroups()
    {
        return $this->userService->getGroups();
    }
}
