<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\IGroup;
use App\Repositories\Eloquent\Criteria\LatestFirst;
use Illuminate\Http\Request;
use App\Http\Requests\GroupRequest;

use App\Models\User;
use phpDocumentor\Reflection\Types\This;

class GroupController extends Controller
{
    protected $groups;

    public function __construct(IGroup $groups)
    {
        $this->middleware('auth');
        $this->groups = $groups;
    }

    public function getIndex(Request $request)
    {
        if ($request->user()->can('manage-group')) {
            return view('front-end.group.index');
        }
        return redirect()->back()->with([
            'flash_level'   => 'warning',
            'flash_message' => trans('action.role_log')
        ]);
    }
    public function index()
    {
        return $this->groups->withCriteria(
            new LatestFirst()
        )->datatable();
    }
    public function getAdd(Request $request)
    {
        if ($request->user()->can('manage-group')) {
            $users = User::select('name', 'id')->get();
            return view('front-end.group.add', compact('users'));
        }
        return redirect()->back()->with([
            'flash_level'   => 'warning',
            'flash_message' => trans('action.role_log')
        ]);
    }

    public function postAdd(GroupRequest $request)
    {
        $groups = $this->groups->create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'user_ids' => json_encode(explode(',', $request->input('user_ids')))
        ]);
        return redirect()->route('front-end.group.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.add_success')
        ]);
    }

    public function getEdit($id, Request $request)
    {
        if ($request->user()->can('manage-group')) {
            $item = $this->groups->find($id);
            $user_selected_ids = (array) json_decode($item->user_ids);
            $users = User::select('id', 'name')->get();
            return view('front-end.group.edit', compact('item', 'users', 'user_selected_ids'));
        }
        return redirect()->back()->with([
            'flash_level'   => 'warning',
            'flash_message' => trans('action.role_log')
        ]);
    }

    public function postEdit($id, GroupRequest $request)
    {

        $this->groups->update($id, [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'user_ids' => json_encode(explode(',', $request->input('user_ids')))
        ]);
        return redirect()->route('front-end.group.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.update_success')
        ]);
    }

    public function postDelete(Request $request)
    {
        if ($request->user()->can('manage-group')) {
            $this->groups->delete($request->id);
            return response()->json([
                'status' => 200
            ]);
        }
        return response()->json([
            'status' => 403,
            'message' => trans('action.role_log')
        ]);
    }
}
